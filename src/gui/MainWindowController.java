/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;


import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import timeutils.MeasureTime;

/**
 * FXML Controller class
 * TODO - do zrobienia
 * - usuwanie z listy czasów pozycji usuniętych na liście (aktualnie brak takiej opcji)
 * - stworzenie alarmu (na podstawie klasy Timer)
 * - spróbować dodać odgyrywanie dźwięku w przypadku wystąpienia alarmu
 * - zapis i odczyt listy alarmów z zapisanej listy (plik)
 * - pokazywanie aktualnego czasu i daty
 * @author adminteb
 */
public class MainWindowController implements Initializable {

    //przechowuje informacje o opisie czasów - zmienna analogiczna do tej z Console.java
    final Map<String,String> timeDescription = new HashMap<>();
    
    /**
     * Klasa stworzona celem rozbudowania podstawowej funkcjonalności kontrolki TextField
     * Dzięki niej możliwe jest przechowywanie referencji do obiektów nadrzędnych
     * (rodzica - listText) oraz bliźniaka-rodzeństwa (cellMainElement)
     */
    class CellListView extends TextField {
        Node listText,cellMainElement;            
          
        public Node getListTextParent() {return listText;}
        public Node getCellParent() {return cellMainElement;}       
        
        public CellListView(String text, Node listText, Node cellMainElement) {
            this.setText(text);
            this.listText=listText;
            this.cellMainElement=cellMainElement;

        }        
    }
    
    /**
     * Analogicznie utworzona klasa, w tym wypadku dla przycisku na liście (możliwość
     * usunięcia czasu z listy).
     */
    class CellListButton extends Button {
        Node cellMainElement;
        public Node getCellParent() {return cellMainElement;}
        public CellListButton(String text, Node cellMainElement) {
            this.setText(text);
            this.cellMainElement=cellMainElement;
        }
    }
    //dzięki adnotacji FXML obiekt timeList z pliku MainWindow.fxml dostepny jest dla kontrolera
    @FXML ListView timeList;
    //analogicznie działają poniższe połączenia
    @FXML
    Text outputText;
    
    @FXML TextArea timeDesc;
    
    @FXML ComboBox timeBox;
    //utworzona DEKLARACJA do pomiaru czasu; w późniejszym czasie zostanie przekazana
    //odpowiednia referencja zostanie dodana później (metodą)
    MeasureTime mt;
    
    /**
     * Initializes the controller class.
     * @param url adres używany do rozwiązywania lokalizacji dla głównego obieku (w naszym wypadku będzie to ścieżka MainWindow.fxml)
     * @param rb zmienna zarezerowoana dla określonych celów w aplikacji, np. do tłumaczenia interfejsu (przykład pod adresem
     * https://docs.oracle.com/javase/8/docs/api/java/util/ResourceBundle.html)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //poniższy wpis zastąpi zdefiniowane w pliku fxml elementy listy; można go zakomentować by zobaczyć różnicę
        timeList.setItems(FXCollections.observableArrayList(addTimeItem("dummyList")));
        //ustawia pierwszy element na liście aktywnym (wybranym)
        timeList.getSelectionModel().select(0);
        
    }    
    
    /**
     * Metoda tworzy połączenie pomiędzy obiektem programu głównego MeasureTime
     * z naszym kontrolerem (by można było sterować czasem). Jeżeli nie dodalibyśmy
     * tego elementu w ten sposób (referencja do orygiały - nie jest to typ prosty)
     * kontroler musiałby posiadać własny obiekt czasowy. Nie jest to jednak idea kontrolera
     * (ma on jedynie operować na obiektach aplikacji, a nie je tworzyć i utrzymywać!)
     * @param o 
     */
    public void setParentMeasureTime(MeasureTime o) {
        this.mt = o;
        //na nowo czyścimy listę i  tworzymy listę taką, jaka znajduje się w obiekcie MeasureTime
        timeList.setItems(FXCollections.observableArrayList(mt.getTimesName().stream().map(mapper->addTimeItem(mapper)).collect(Collectors.toList())));
        //ponownie wybieramy domyślny element
        timeList.getSelectionModel().select(0);
    }
    /*
    Poniższe metody odpowiadają za połączenie zdarzeń z pliku fxml z odpowiednimi operacjami 
    w programie. Wszystkie metody łączące się ze zdarzeniami elementów-kontrolek 
    musza mieć adnotację FXML!
    */
    @FXML 
    protected void startTime(MouseEvent e) {
        mt.startMeasure(((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText());
    }
    
    @FXML
    protected void holdTime(MouseEvent e) {
        String timeName = ((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText();
        if (mt.getRunningStatus(timeName)) {
            mt.suspendMeasure(timeName);
            
        }
        else {
            if (mt.getTimesName().contains(timeName)) 
                mt.activateMeasure(timeName);
            else startTime(e);
        }
    }
    
    @FXML 
    protected void stopTime(MouseEvent e) {
        mt.stopMeasure(((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText());
    }    
    
    @FXML
    protected void endApp(MouseEvent e) {
        System.out.println("Wyście!!" + e.getX());
        System.exit(0);
    }
    
    @FXML
    protected void listChange(MouseEvent e) {
        if (e.getClickCount()==2) {
            Node tmp = addTimeItem();
            timeList.getItems().add(tmp);
            timeList.getSelectionModel().select(tmp);
        }
        else {
            timeList.getItems().removeAll(
                    timeList.getItems().filtered(predicate->((CellListView)((HBox)predicate).getChildren().get(0)).getText().isEmpty()).toArray());
        }
        checkTimeDescription();
    }
    
    @FXML protected void textAreaChange(KeyEvent e) {
        String timeName = ((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText();
        if (timeDescription.keySet().contains(timeName)) {
            timeDescription.replace(timeName, ((TextArea)e.getSource()).getText());
        }
        else
            timeDescription.put(timeName, ((TextArea)e.getSource()).getText());
    }
    
    //metoda, która będzie wywoływana w programie głównym (plik StopWatch.java)
    //odpowiada za wyświetlanie mierzonego czasu, który aktualnie jest zaznaczony
    //na liście
    public void writeCurrentTime() {
        String timeName = ((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText();
        //sprawdzamy czy czas z podaną nazwą istnieje w Mapie MeasureTime.java
        if (mt.getTimesName().contains(timeName)) 
            outputText.setText(mt.getFormattedMeasure(
                    timeBox.getSelectionModel().getSelectedIndex(), 
                    ((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText()));
        //jeżeli nie wyświtlamy stosowany komunikat
        else
            outputText.setText("Brak pomiaru czasu dla wybranego elementu");
    }
    
    /*
    metoda odpowiada za sprawdzenie czy wybrany czas posiada opis
    jeżeli tak wyświetla go, w przeciwnym razie kasuje zawartość pola
    */
    void checkTimeDescription() {
        String timeName = ((CellListView)((HBox)timeList.getSelectionModel().getSelectedItem()).getChildren().get(0)).getText();
        if (timeDescription.keySet().contains(timeName)) {
            timeDesc.setText(timeDescription.get(timeName));
        }
        else
            timeDesc.clear();
    }
    
    //metoda dodaje do listy nowy czas; należy zauważyć, że do listy 
    //dodawany jest element HBox, nie ciąg znakowy; jak widać lista może zawierać
    //skomplikowane formy obiektów, nie tylko np. ciąg znakowy bądź prosty element
    Node addTimeItem() {
        return addTimeItem("");
    }
    
    Node addTimeItem(String text) {
        //utowrzenie elementu HBox zawierającego docelowo dwa inne komponenty
        HBox hbox = new HBox();
        //utworzenie elementu tekstowego zmodyfikowanego wcześniej (klasa CellListView)
        //jest w pełni kompatybilna z TextField (dlatego element pojawia się  
        //w GUI)
        CellListView tf = new CellListView(text,timeList,hbox);
        //analogicznie działa jak inicjalizacja powyżej
        CellListButton btn = new CellListButton("X",tf);
        //dodanie przyciskowi odpowiedniego zdarzenia, które pozwala na usunięcie 
        //wybranego elementu z listy czasów
        btn.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((ListView)((CellListView)((CellListButton)event.getSource()).getCellParent()).getListTextParent()).getItems().
                        remove((HBox)((CellListView)((CellListButton)event.getSource()).getCellParent()).getCellParent());                
            }
        });
        //w ten sposób nadaje się elementom nazwę klasy z CSS, dzięki której będzie odpowiednio wyglądać
        tf.getStyleClass().add("listText");
        //metoda, dzięki której ustawiany jest odpowiedni indeks listy oraz sprawdza
        //czy wybrany czas posiada opis
        tf.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //przechwycenie obiektu nadrzędnego
                HBox tmp = (HBox)((CellListView)event.getSource()).getCellParent();
                //wybranie tego obiektu z listy
                ((ListView)((CellListView)event.getSource()).getListTextParent()).getSelectionModel().select(tmp);
                checkTimeDescription();
            }
        });
        //zdarzenie pozwalające na wyczyszczenie listy w przypadku gdy 
        //nie zostanie dodana nazwa czasu (taki czas nie powinien istnieć
        //ZAMIAST anonimowej klasy (widocznej chociażby powyżej) można wykorzystać wyrażenie labda
        //nie trzeba wtedy nadpisywać wykorzystywanych metod (dzieje się to niejawnie)
        tf.onMouseExitedProperty().set((EventHandler<MouseEvent>) (MouseEvent event) -> {
            listChange(event);
        });
        hbox.getChildren().addAll(tf, btn);
        return hbox;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webview;

import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

/**
 *
 * @author adminteb
 */
public class MyWebView extends Application{
    
    //JSObject win;
    WebEngine webEngine;
    Timer time;
    EventListener el;
    
    /*
    poniższe dwie klasy pokazują jak można utworzyć tzw. mostki do komunikacji pomiędzy Java i JavaScript
    zarówno jednak jak i druga zostanie przemieszczona do JavaScript; pierwsza (MyBridge) zostanie umieszczona w nim
    w DOM (Document Object Model) i będzie wymagała wywołania z obiektu document
    
    Druga klasa umieszczona zostanie w BOM (Browser Object Model) i będzi mogła być wywoływana po nazwie
    (domyślnie wybrany jest model window)
    Każda metoda w nich umieszczona będzie mogła być wywołana w JavaScript
    */
    public class MyBridge {
        public void testFunction(String s) {
            System.out.println("Komunikat ze zmiennej DOM: " + s);
        }
    }
    
    public class CommunicationBridge {
        public void testFunction(String s) {
            System.out.println("Komunikat ze zmiennej BOM: " + s);
        }
        
        public void closeApp() {
            System.exit(0);
        }
    }
    
    public static void main(String[] args) {
        
        
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        el = new EventListener() {

            @Override
            public void handleEvent(org.w3c.dom.events.Event evt) {
                System.out.println("Wywołane z JavaScript");
            }
        };
        
        WebView wv = new WebView();
        /*
        WebView może otwierać dowolne strony jako przeglądarka (zakomentowana linia pokazuje 
        możliwość otwarcia adresu google) jak też dowolne pliki z dysku twardego.
        */
        //File f = new File(MyWebView.class.getResource("/webview/example.html")).
        //wv.getEngine().load("https://google.pl");
        webEngine = wv.getEngine();
        //tutaj przykład otwarcia dokumentu bezpośrednio z aplikacji (wkompilowanego w nią)
        webEngine.load(MyWebView.class.getResource("/webview/example.html").toString());
        webEngine.getLoadWorker().stateProperty().addListener((ObservableValue<? extends State> observable, State oldValue, State newValue) -> {
            //poniższy kod jest wykonywany tylko wtedy jeżeli dokument w WebView zostanie załadowany poprawnie
            //jeżeli nie - nie wykona się, a on  z kolei nie wywoła błędów aplikacji

            //można obsłużyć jeszcze stany anulowania, braku dokumentu i kilku innych

            if (newValue==State.SUCCEEDED) {
                primaryStage.setTitle("Aplikacja HTML");                
                JSObject win = (JSObject) webEngine.getDocument();
                win.setMember("winApp", new MyBridge());
                win = (JSObject) webEngine.executeScript("window");
                win.setMember("windowObj", new CommunicationBridge());
                
                time = new Timer();
                time.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        //aby metoda mogła się wykonać musi zostać wykonana w metodzie klasy Platform
                        Platform.runLater(()->{
                            executeCommand();
                        });
                    }
                }, 1000, 1000);
                Document d = webEngine.getDocument();
                Element e = d.getElementById("startButton");
                ((EventTarget)e).addEventListener("click", el, true);
            }   
        });

        primaryStage.setScene(new Scene(wv));
        
        primaryStage.show();
    }
    
    //metoda, która będzie wywoływała metodę z JavaScript (wyświetlenie tekstu po stronie WWW)
    public void executeCommand() {
        String odp = (String) webEngine.executeScript("showInfo('Informacja z Java!')");
        System.out.println(odp);
    }
    
}

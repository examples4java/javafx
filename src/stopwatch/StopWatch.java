/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stopwatch;

import gui.MainWindowController;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import timeutils.MeasureTime;




/**
 *
 * @author adminteb
 */
public class StopWatch extends Application {
    
    public final MeasureTime mt = new MeasureTime();
    
    Timer refreshText;
    
    public static void main(String[] args) {
//        Console console = new Console();
//        console.showMenu();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/MainWindow.fxml"));
        Parent melement = loader.load();//FXMLLoader.load(getClass().getResource("/gui/MainWindow.fxml"));
        //((MainWindowController)loader.getController()).setParentMeasureTime(mt);
        MainWindowController mwc = (MainWindowController)loader.getController();
        mwc.setParentMeasureTime(mt);
        refreshText = new Timer();
        refreshText.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mwc.writeCurrentTime();
            }
        }, 1000, 1000);
        //przykład pokazuje jak nadać WŁASNĄ ikonę aplikacji. Ważne by plik był odpowiedniego rodzaju
        //(nie może być np. ico, png jest najodpowiedniejszy); Ponieważ plik jest dodany do katalogu źródłowego
        //klasa go już posiada; trzeba go tylko pobrać (jak widać na poniższym przykładzie)
        primaryStage.getIcons().add(new Image(StopWatch.class.getResourceAsStream("/indeks.png")));
        //chodziać poniższe nie zadziała (plik ico zamiast png) można też ładować ikony z serwerów i innych lokalizacji
        //primaryStage.getIcons().add(new Image("https://cdn.sstatic.net/Sites/stackoverflow/img/favicon.ico"));
        primaryStage.setTitle("Nowa aplikacja");
        primaryStage.setScene(new Scene(melement));
        
        primaryStage.show();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stopwatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import timeutils.MeasureTime;

/**
 * Klasa odpowiada za działanie programu w trybie konsoli.Pozwala na wybieranie dowolnej z opcji
 * wyświetla rónicę liczonych czasów oraz pozwala na dodawanie wielolinowych opisów czasu
 * @author adminteb
 */
public class Console {
    
    String timeName;
    MeasureTime mt;
    
    Map<String,String> timeDescription;
    
    public Console() {
        timeName = "default";
        mt = new MeasureTime();
        timeDescription = new HashMap<>();
    }
       
    //wyłączenie ostrzeżenia kompilatora o zapętlonej rekurencji
    @SuppressWarnings("InfiniteRecursion")
    public void showMenu() {
//        try {
//            System.setOut(new PrintStream(System.out, true, "utf-8"));
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(StopWatch.class.getName()).log(Level.SEVERE, null, ex);
//        }
        clrscr();
        System.out.println("Czasomierz. Kliknij: ");
        System.out.println("1 - rozpoczęcie liczenia");
        System.out.println("2 - podaj nazwę mierzonego czasu");
        System.out.println("3 - zakończ działanie mierzenia czasu");
        System.out.println("4 - podaj czas mierzony (bądź zmierzony) aktualnie wybranego czasu");
        System.out.println("5 - wypisz aktualny opis wybranego czasu (o ile dostępny)");
        System.out.println("6 - zmień wybrany czas");
        System.out.print("Dowolna inna liczba zakończy działanie aplikacji. Twój wybór: ");
        
        //System.out.println(new InputStreamReader(System.in).getEncoding());
        //System.out.println(readMenu());
        switch (readMenuInt()) {
            case 1: System.out.println("Wybrałeś rozpoczęcie liczenia czasu \"" + timeName + "\"!"); mt.startMeasure(timeName); break;
            case 2: setTimeName(); break;
            case 3: mt.stopMeasure(timeName); System.out.println("Zakończyłeś liczenie czasu \""+ timeName + "\"!"); break;
            case 4: System.out.println("Aktualna różnica mierzonego czasu " + timeName + " wynosi: " + mt.getMeasure(timeName)); break;
            case 5: if (timeDescription.keySet().contains(timeName)) System.out.println(timeDescription.get(timeName)); else System.out.println("Brak opisu dla czasu."); break;
            case 6: changeCurrentTime(); break;
            default: System.exit(0);
        }
        //wyłączenie ostrzeżenia dotyczy poniższej linii, która to zapętla program 
        //w  nieskończoność (aby użytkownik mógł korzystać z programu w nieprzerwany sposób)
        readString();
        showMenu();
    }
    
    void changeCurrentTime() {
        Integer counter[]= new Integer[1];
        counter[0] = 0;
        System.out.println("Proszę wybrać jeden z czasów na liście: ");
        mt.getTimesName().forEach(n->System.out.println((counter[0]++) + " " + n));
        System.out.println("Wybór to: ");
        try {
            timeName = mt.getTimesName().get(readMenuInt());
        }
        catch (Exception e) {
            System.err.println("Popełniłeś błąd lub wybranego czasu nie ma na liście! Zmiana czasu nie została dokonana!");
            readString();
        }
        showMenu();
    }
    
    void setTimeName() {
        System.out.print("Proszę podać nazwę czasu. Wpisanie znaku ? wyświetli wszystkie dostępne czasy: ");
        String s = readString();
        if (s.isEmpty()) {
            System.err.println("Podałeś pusty ciąg znakowy, z którego nie można utworzyć zapisu!");
            readString();
            showMenu();
        }
        //zagnieżdżenie switch-case (niekonieczne, ale pokazuje możliwość tworzenia tego typu
        //kodu; oczywiście nie jest to specjalnie zalecane)
        switch(s) {
            case "?": mt.getTimesName().forEach(n->System.out.println(n));
                      setTimeName();
            default: timeName=s; System.out.print("\nCzy chcesz dodać opis czasu [t/N]?: ");
                        switch(parseAnswer(true, "tak"))
                        {
                            case "tak": System.out.println("\nWpisz opis dodawanego czasu"); 
                                        if(!timeDescription.keySet().contains(timeName)) 
                                            timeDescription.put(timeName, readText());
                                        else 
                                            timeDescription.replace(timeName, readText()); 
                            //można dodać przypadek na nie; domyślnie go brak
                            default: System.out.println("\nCzas dodano do listy (bądź zastąpiono). Można go używać!"); 
                                readString();                                
                                showMenu();
                        }
        }
    }
    
    //czyszczenie konsoli systemowej (by nie zaśmiecać ekranu niepotrzebnymi danymi)
    //metoda wykorzystywana przez showMenu()
    public static void clrscr(){
    //Clears Screen in java
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {}
        System.out.println();
    }   
    
    String parseAnswer(boolean toLower, String... s) {
        String r = readString();
        r = (toLower)? r.toLowerCase() : r.toUpperCase();
        for (String ss:s) {
            String help = (toLower)? ss.toLowerCase() : ss.toUpperCase();
            if ((help.contains(r) || r.contains(help)) && !r.isEmpty()) return ss;
        }
        return "";
    }
    
    /*
    Poniżze metody pokazują w jaki sposób Java obsługuje wejście (w tym wypadku
    klawiatury) bez klasy Scanner. Każdy impuls zamieniany jest na bajt (bądź bajty),
    które następnie można obrobić w dowolny sposób (np. zamienić na ciąg znakowy, a 
    z niego na dowolną inną wartość)
    */
    
    /**
     * Metoda mam za zadanie pobrać wartość całkowitą ze strumienia wejściowego
     * @return zwraca podaną przez użytkownika liczbę całkowitą bądź -1 w przypadku
     * podania wartości innej niż oczekiwano
     */
    int readMenuInt() {
        try {
        //gdyby w readString nie został usunięty znak nowej linii
        //zakomentowana zawartość byłaby potrzebna (bez usunięcia tego znaku 
        //wyskoczyłby wyjątek niemożności konwersji na liczbę)
            return Integer.parseInt(readString()/*.split("\n")[0]*/);
        }
        catch (NumberFormatException e) {
            System.err.println("Błąd! Wpisałeś znak zamiast liczby (bądź szereg znaków). Wpisz poprawne dane!");
            showMenu();
        }
        return -1;
    }
    
    /**
     * Metoda pobiera od użytkownika ciąg znaków. Czytanie strumienia wejściowego
     * trwa aż do chwili gdy na wejściu będą dostępne kolejne bajty (kolejne impulsy z
     * klawiatury)
     * @return pobrany z wejścia ciąg znakowy pozbawiony ostatniego znaku - początku nowej linii
     * (w większości przypadków nie jest pożądany i powoduje sporo problemów np. przy zamianie
     * na liczbę)
     */
    String readString() {
        //wszystkie przeczytane bajty odkładamy na listę
        List<Byte> byteArray = new ArrayList<>();
        //czytamy bajt po bajcie, dlatego wielkość tablicy ustawiona została na 1
        byte inByte[] = new byte[1];
        try {
            //czytamy wejście do chwili gdy mamy dostępne informacje (available większe od 0)
            do {
                System.in.read(inByte);
                byteArray.add(inByte[0]);
            } while (System.in.available()>0);
        } catch (IOException ex) {
            Logger.getLogger(StopWatch.class.getName()).log(Level.SEVERE, null, ex);
        }
        //lista przydałą się do dynamicznego zapisu danych; teraz potrzebna będzie tablica
        //zamiana na tablicę odbywa się poprzez podanie jej rozmiaru w parametrze toArray;
        //może to być pusta lista (jak poniżej) - metoda zwraca odpowiednią tablicę do zmiennej (przypisanie wartości)
        Byte tmpChars[] = byteArray.toArray(new Byte[byteArray.size()]);
        //niestety będziemy potrzebować tablicy prostych zmiennych - nie obiektów
        byte strBytes[] = new byte[byteArray.size()-1];
        int it=0;
        //usunięcie ostatniego przechiwyconego znaku - \n; znak ten powoduje
        //sporo problemów m. in. przy zamianie na liczbę
        for (byte bp:tmpChars) {if(it==strBytes.length) break; strBytes[it++]+=bp;}
        //możemy zakończyć metodę poprzez zwrócenie ciągu znakowego utworzonego z tablicy bajtów
        //dodatkowo upewniamy się, że nasz ciąg znakowy będzie posiadał kodowanie UTF-8
        return new String(strBytes, StandardCharsets.UTF_8);
    }
    
    /**
     * Metoda pozwala na pobranie wieloliniowego zapisu z konsoli i zapisaniu tychże
     * informacji do ciągu znakowego. W zasadzie działanie to mogłaby przejąć poprzedmia metoda.
     * Chodzi jednak o pokazanie potencjału innego podejścia (utworzenie obiektu typu BufferedReader)
     * @return ciąg znakowy wieloliniowy
     */
    String readText() {
        
        String strRet = new String();
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(System.in, new InputStreamReader(System.in).getEncoding()));
            //poniżej komentarz try - catch zachowany by pokazać, że można tego typu konstrukcje zagnieżdżać;
            //w tym wypadku jednak został ujednolicony jeden try-catch
//            try {
              String s;
              //do chwili, gdy będzie pobierana nowa linia i nie będzie to pusta linia
              //do tego momentu będzie działała poniższa pętla
              while(!(s = br.readLine()).isEmpty()) strRet+=s+'\n';
//
//            } catch (IOException ex) {
//                Logger.getLogger(StopWatch.class.getName()).log(Level.SEVERE, null, ex);
//            }
        } catch (IOException ex) {
            Logger.getLogger(StopWatch.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strRet;
    }
}
